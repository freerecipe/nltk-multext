% !TeX spellcheck = en_US
\chapter{Evaluation}\label{evaluation}
\label{evaluation:10fold}
For evaluating the performance of the taggers we use a 10-fold-cross validation approach. The data is split into ten, approximately equal, parts. In each run another part is used for testing the tagger while the rest is used for training. The results are averages of the ten individual runs.

\section{Evaluation Setup and Environment}
In the evaluation we used the corpus reader and the taggers implemented by us that were described in former sections. The complete evaluation was run with Python 3.4 due to encoding issues we encountered with earlier versions. \nltk{} was used with version 3.0.4 and \scikit{} in version 0.16.1. 

For our Brill-Tagger implementation we used four template sets which are pre-defined by \nltk{}:
\begin{itemize}
    \item nltkdemo18
    \item nltkdemo18plus
    \item fntbl37\footnote{based on the fntbl-distribution: \url{http://www.cs.jhu.edu/~rflorian/fntbl/index.html}}
    \item brill24\footnote{based on the work of Eric Brill~\cite{Brill:1995:TEL:218355.218367}}
\end{itemize}
The context windows used for the \scikit{}-based taggers are not pre-defined but specified by us, there may be others or evem combinations of context windows such that the taggers would perform better, but testing more context windows or their combinations was not possible within the scope of this task. Therefore we focused on the ten most common windows, from one to three words left, right or around from the current word (plus the current word) and only the current word. An overview over all configurations and their abbreviations used later on can be seen in Table~\ref{table:shorts}.

The evaluation was run on a computer with 32\,GB of RAM and an Intel(R) Core(TM) i7-4771 CPU @ 3.50GHz with our evaluation framework described earlier.


\section{Metrics}
Accuracy, precision, recall and f1-score are measured by the appropriate methods within \nltk. They calculate the micro averaged values: The global results of the false positive ($f_p$), false negative ($f_n$) and true positive ($t_p$), true negative ($t_n$) are calculated by summing up those values for each class~\cite{eval:sebastiani}.\\
When we talk about the \textit{best} performance of a tagger we always think of the f1-score.

\paragraph{Accuracy} is the number of true results divided by the number of all data: 
\begin{displaymath} \frac{t_p+t_n}{t_p+t_n+f_p+f_n} \end{displaymath}
If accuracy is 1.0 all predicted values are the same as the gold standard.


\paragraph{Precision} is the number of true positives (items correctly labeled as belonging to the positive class) divided by the total number of elements belonging to the positive class: \begin{displaymath}\frac{t_p}{t_p+f_p}\end{displaymath} 
If precision is 1.0 the classifier has labeled all data (for this class) correctly. 


\paragraph{Recall} is the number of true positives divided by the total number of elements that belong to the positive class: 
\begin{displaymath} \frac{t_p}{t_p+f_n} \end{displaymath}
If recall is 1.0 the classifier as labeled all data as belonging to the right class. But recall does not contain any information about the number of data that is labeled as the wrong class (false positives)

\paragraph{F1-Score} describes the harmonic mean of precision and recall and can be seen as weighted averaged between those two metrics:

\begin{displaymath} F_1 = 2 \cdot \frac{\mathrm{precision} \cdot \mathrm{recall}}{\mathrm{precision} + \mathrm{recall}} \end{displaymath}


\paragraph{Out-of-Vocabulary words}\label{metrics:oov} is the number of words, that appear in the test corpus but not in the training corpus and thus are \textit{new} to the tagger. This number does not change while evaluating with different tagsets as the partitions of training- and test-sets in the cross-fold validation does not depend on the tagset but only on the outline of the corpus.

\paragraph{Spearman's rank correlation coefficient}\footnote{Further reading \cite{spearman1904}} is a nonparametric measure of the statistical (in)depence of two variables. It answers to the question "how well can a relationship between those to variables described by a monotonic function". If the data fits perfectly to a monotonic function, the result itself will be $-1$ or $1$. 

The coefficient itself is defined as the Pearson correlation coefficient between the ranked data points. It is calculated for each data point $x_1$ and $y_1$, with $d_i = x_i - y_i$  the difference between the ranks and $n$ the number of data point tuples.

\begin{displaymath}\rho = {1- \frac {6 \sum d_i^2}{n(n^2 - 1)}}\end{displaymath}

The correlation can be interpreted as the association between the independent variable $X$ and the depended one $Y$. If both variables increase the $\rho$, the sign of the Spearman correlation coefficient is positive, but if $Y$ decreases while $X$ increases $\rho$ is negative. If there is no tendency that $Y$ will increase or decrease when $X$ is increasing, the coefficient  $\rho$ is zero. This is interpreted as a very low correlation between the data points.



\clearpage


\section{Results}
In this section we will have a short look at each implemented tagger and each \multexteast~language. We will analyse the most significant parts of the results in this section. Information about the raw data can be found in Section~\ref{eval:raw}.
For better readability of tables and plots we decided to create abbreviations for the tested configurations. The mapping of configuration to abbreviaton  and a description of the configuration can be seen in Table~\ref{table:shorts}. 
 
\begin{table}
\begin{minipage}[t]{.48\textwidth}
\resizebox{\textwidth}{!}{
\begin{tabular}{|l|l|l|}
\hline
	\textbf{Configuration} & \textbf{Abbreviation} & \textbf{Description} \\
	 \hline baseline & base & Baseline (NLTK Unigram Tagger) \\
	 \hline [0] & base & Baseline (Current word) \\
		 
	 \hline brill24 & br24 & Brill 24\\
	 \hline fntbl37 & ftbl37 & Fnt Tables 37 \\
	 \hline nltkdemo18 & nl18 & NLTK Demo Ruleset\\
	 \hline nltkdemo18plus & nl18+ & NLTK Demoe Rulset  \\
	 \hline [-3, -2, -1, 0, 1, 2, 3] & arnd3 & Three words\\
	 \hline [-2, -1, 0, 1, 2] & arnd2 & Two words around\\
	 \hline [-1, 0, 1] & arnd1 & One word around\\
	 \hline [-1, 0] & left1 & One word left from\\
	 \hline [-2, -1, 0] & left2 & Two words left\\
	 \hline [-3, -2, -1, 0] & left3 & Three words left \\
	 \hline [0, 1] & right1 & One word right\\
	 \hline [0, 1, 2] & right2 & Two words right\\
	 \hline [0, 1, 2, 3] & right3 & Three words\\
\hline
    \end{tabular}} 
  \caption{Mapping of the Name of Configuration Options to Abbreviations}
  \label{table:shorts}
\end{minipage}
\begin{minipage}[t]{.48\textwidth}
\resizebox{\textwidth}{!}{
\begin{tabular}{|l|d{2.4}|d{2.4}|d{2.4}|d{0.4}|}
\hline
			\textbf{Language} & \multicolumn{1}{c|}{\textbf{Minimum}} & \multicolumn{1}{c|}{\textbf{Maximum}} & \multicolumn{1}{c|}{\textbf{Average}} & \multicolumn{1}{c|}{\textbf{Standard Deviation}} \\
			\hline Czech & 15.3895 & 16.9205 & 15.8649 & 0.4397 \\
			\hline English & 4.3627 & 5.1983 & 4.7758 & 0.2470 \\
			\hline Estonian & 15.3014 & 17.2707 & 16.2746 & 0.6795 \\
			\hline Farsi & 5.3365 & 6.9215 & 6.1749 & 0.4515 \\
			\hline Hungarian & 16.3756 & 18.5125 & 17.5267 & 0.5290 \\
			\hline Polish & 16.9517 & 18.2349 & 17.7253 & 0.4454 \\
			\hline Romanian & 8.2533 & 8.9866 & 8.6080 & 0.2502 \\
			\hline Slovak & 15.3369 & 17.0994 & 16.4197 & 0.5002 \\
			\hline Slovenian & 12.2008 & 13.3773 & 12.8194 & 0.4113 \\
			\hline Serbian & 12.0504 & 13.7943 & 13.0699 & 0.5891 \\
\hline
 	\end{tabular}}
 	\caption{Out of Vocabulary Words per Language}
 	\label{table:oov_words}
\end{minipage}
\end{table}
When it comes to out-of-vocabulary statistics -- which are independant of configuration and used tagger -- the differences are huge. Where in the Czech version approximately 15.8\,\% words are not known from the training set, in English only 4.7\,\% are out-of-vocabulary. This has to be taken into consideration when drawing conclusions about the performance of the taggers on different languages.
In the following sections we will at first compare all languages per tagger and afterwards compare all taggers per language.


\subsection{Analysis per Tagger}
\input{parts/eval_per_tagger_overall}
\input{parts/eval_per_tagger}


\subsection{Analysis per Language}
\input{parts/eval_per_language_overall}
\input{parts/eval_per_language}


\subsection{Raw Data}\label{eval:raw}
The raw data is csv file which contains the results of every evaluation run. The first row is the tagger which produced the result, the second one indicates the used corpus, the third one the tagset, the fourth one represents the configuration option, the fifth one is the evaluated metric, followed by minimum, maximum and averaged value, the last row is the standard deviation.

\paragraph{Taggers} are named by the evaluation class of the framework. \texttt{skPerceptron} indicates that \texttt{sklearn.linear\_model.Perceptron} with \texttt{n\_iter=50} was uses. \texttt{skMultinomialNB} uses \texttt{sklearn.naive\_bayes.MultinomialNB} with the default configuration. \texttt{skLinearSVC} instantiates \texttt{sklearn.svm.LinearSVC} with the standard settings for tagging. \texttt{nltkBrill} shows that our implementation of the \nltk{}-brill tagger was used. \texttt{tmpOOV} is a virtual evaluation to calculate the out-of-vocabulary words.

\paragraph{Name of the Language} can be derived from filename of the \multexteast- distribution. The files are named as the following scheme: oana-\textit{ISOCODE}.xml

\paragraph{Tagset} can either be \texttt{mte} for the \textit{MSD} tagset or \texttt{universal} for the \textit{universal} tagset.

\paragraph{Configuration Option} indicates the configuration for the tagger which was used. Since each implementation can define its own options we give a short overview about the \scikit{} taggers (tagger-name starts with \textit{sk}), the \nltk{}-brill-tagger and our helper implementation.

\subparagraph{\scikit{}-Taggers} are configured by a tuple. The first value indicates the context window, which is relative to the current word (e.g. \texttt{[0,1]} indicates that the current word (\textit{0}) and the word after it (\textit{1}) is used.)

\subparagraph{\nltk{}-Brill-Tagger} can use four tables for evaluation
\begin{itemize}
	\item \textbf{fntbl37}
	\item \textbf{brill24}
	\item \textbf{nltkdemo18}
	\item \textbf{nltkdemo18plus}
	\item \textbf{baseline}: This represents the baseline and is generated by the NTLK Unigram Tagger
\end{itemize} 

\subparagraph{tmpOOV} provides only the \texttt{mkOOV} option which calculates the out-of-vocabulary words for the given fold.

\paragraph{Metrics} are calculated for each fold and summed up. For each combination we gather the following metrics: \texttt{accuracy}, \texttt{recall}, \texttt{precision}, \texttt{f1}
\texttt{oov}, \texttt{training\_time}, \texttt{prediction\_time}. If implementation does not support the metric an default value of $-1$ will be returned.

\paragraph{Minimum Value} of all results from the fold is stored in this row.
\paragraph{Maximum Value} of all results form the fold is stored in this row.
\paragraph{Averaged Value} of all results form the fold is stored in this row.
\paragraph{Standard Deviation} of the averaged value is stored in this row.

