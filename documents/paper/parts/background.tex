% !TeX spellcheck = en_US
\chapter{Background}
\section{Natural Language Processing Toolkit}
\nltk{}~\cite{nltk}, is a flexible python framework for projects related to language processing and text mining. It contains various different corpora, such as the Brown Corpus\footnote{See \url{http://clu.uni.no/icame/manuals/BROWN/INDEX.HTM}.} or WordNet~\cite{wordnet}. 
The UDHR corpus\footnote{See \url{http://research.ics.aalto.fi/cog/data/udhr/}.} is one of the few multilingual corpora in \nltk{}, it comes without POS-Tags.\\
These corpora can be inspected and processed with pre-implemented methods and algorithms, that range from computing simple frequency distributions of words in a text to complete Part-of-Speech Taggers. For visualizing the findings \nltk{} allows to plot arbitrary graphs \footnote{Further use-cases and exemplary python code can be found in the first two chapters of the book \textit{Natural Language Processing with Python}~\cite{nltkbook}}.\\
\mbox{\textit{Penn Treebank II}} and \mbox{\textit{Universal Part-of-Speech Tagset}}~\cite{universal} are the most commonly used tagsets inside \nltk{}.
While the Penn Treebank II tagset is quite precise and trimmed to English language only, the universal tagset is more abstract, applicable to many languages, and can therefore only provide coarse information about the type of a word in a sentence, in Section~\ref{tagsets} more detailed information about the different tagsets can be found.
A quite easy-to-implement POS-Tagger is the Brill-Tagger (c.f. Section~\ref{background:tagger}) from \nltk{}. It works with arbitrary tagsets, as well as different languages.

\section{Multext-East}
\multexteast{} is a spin-off of the \multext{} project which was made in the late 90's. \multexteast{} is a corpus consisting of morphosyntactic information and POS-Tags of George Orwell's book \textit{1984} for the languages Bulgarian, Czech, English, Estonian, Persian, Hungarian, Macedonian, Polish, Romanian, Slovak, Slovenian and Serbian~\cite{multex-east}. The broad spectrum of available languages and the fact that there are POS-Tags and the lemma for each word in the book for each included language makes the corpus quite interesting and unique.\\
The corpus is publicly available at the Slovenian homepage of the \textit{Common Language Resources and Technology Infrastructure}\footnote{It is available under \textit{CC BY-NC-SA 4.0} license~\cite{clarinMul}.}.
The corpus contains a file for each language (\texttt{LL}) with annotations on word-level. They are named \texttt{oana-LL.xml}. Additionally there are files \texttt{oalg-LLMM.xml} which include the sentence alignments of the language \texttt{LL} to a language \texttt{MM}. Besides these files there are some xml header files and a folder containing the schmea, used for encoding the xml files, it is compliant to the TEI P5~\cite{tei-p5} xml scheme.
The basic structure of the \texttt{oana} files is as follows: 
\begin{itemize}
\item At first there is a header, specific for each language, which contains an abstract, authors, copyright information and change notes,
\item then, in the body of the xml file the annotated version of the book \textit{1984} can be found. It is structured in paragraphs (\texttt{<p>}), sentences (\texttt{<s>}) and words / punctuation marks (\texttt{<w>}, \texttt{<c>}). Paragraphs as well as sentences and punctuation marks only have an id attribute, furthermore paragraphs contain one or more sentences and a sentence may contain words and punctuation marks. A word has a \texttt{lemma} and an \texttt{ana} attribute, where the lemma and the POS-Tag annotation of the word can be found. \footnote{There are two languages, bulgarian and macedonian, whose xml scheme differs from the presented structure, more on that can be found in Section~\ref{restrictions}.}
\end{itemize}
The tagset is feature-structure based and even more fine-grained than the Penn Treebank II tagset. It is represented by a string consisting of characters at certain positions, e.g. \texttt{\#N} for a noun, unknown or inapplicable parts of tags can be symbolized by \texttt{-} or just left out if they only occur at the end, e.g. \texttt{\#Npfs} (a \texttt{f}eminine \texttt{p}roper \texttt{N}oun in \texttt{s}ingular form (e.g. Julia)) is just more detailed than \texttt{\#N}\footnote{All possible tags are listed language-wise on \\ \url{http://nl.ijs.si/ME/V4/msd/html/index.html}.}. From the morphosyntactic descriptions in the tags also comes the name of the tagset: \textit{MSD Tagset}. More about this tagset can be found in Section~\ref{tagsets}.


\section{Part-of-Speech Tagging, Brill-Tagger\label{background:tagger}}
By determining the part of speech of a word in a given context, one gets useful information about the analyzed text. There are several approaches, supervised and unsupervised, to compute the POS-Tags automatically. The so gained information can for example be used for machine translation or information retrieval~\cite{postag}.
One of the most commonly known supervised POS-Taggers is the Brill-Tagger~\cite{brilltagger}. It is a rule-based part of speech tagger proposed by Eric Brill in 1992.
During training, the tagger first uses an initial tagger to generate tags. In our implementation we use an unigram-tagger for this purpose. Afterwards the tagger tries to find rules that transform the tag from the initial tagger into the correct tags. These rules are created based on a set of templates. After training, the tagger saves the rules to use them for tagging a corpus.\\
When the tagger tags an untagged corpus, it first uses the initial tagger and then transforms the tags according to the previously learned rules.


\section{Machine Learning, scikit-learn}\label{background:mlscl}
Machine learning describes the process of algorithmic knowledge generation by analyzing a given set of training data. The deducted rules have to be abstract enough to cover new data, otherwise the algorithm is over-approximating.

\paragraph{\scikit{}~\cite{scikit-learn}} is a machine learning framework for Python and built on NumPy, SciPy and matplotlib. It is open source and BSD-Licensed. It offers a wide range of mechanisms and helpers for classification, regression, clustering and dimensionality reduction. Part-of-Speech tagging is a multi class classification task. This has to be supported by the classifiers.%TODO: cite


\paragraph{Naive Bayes} classifiers are based on a naive implementation of the Baysian-Theorem. Such a classifier assumes that all attributes are independent of each other. A multinomial naive Bayes is useful where the frequencies have been generated by a multinomial, such as in natural language processing.
It's functionality for an implementation in \scikit{} is described in related literature~\cite{mrps:inir}. 
McCallum and Nigam analyse the performance of a multi-variant Bernoulli model and a multinomial one~\cite{amckn:cmpnp}. Their result is that a multinomial one, compared to the Bernoulli model,  provides an average reduction of 27,\% in error.
In training and classification a baysian classifier has a complexity of $O(n_{features} \cdot n_{samples})$~\cite{nb:comp2}.

\paragraph{Support Vector Machines} based classifiers represent the data as vectors within a n-dimensional vector room. The task is to find a hyperplane which separates the vectors in such a way that the margin between the hyperplane and the vectors is maximal \cite{b:igsvm}.
If it is not possible to find a hyperplane which separates the data in the expected way, a so called kernel-trick is used: The vector room will be extended to a higher dimension until a sufficient hyperplane is found~\cite{svm:kernel}.
For a high amount of features this task is quite complex (a problem of quadratic programming) and thus the complexity is between $O(n_{features} \cdot n^2_{samples})$ and $O(n_{features} \cdot n^3_{samples})$~\cite{svm:libsvm}.


\paragraph{Perceptrons} are simplified artificial neuronal networks which can be used as linear classifier~\cite{rojas_1996}. Within this network, each neuron is assigned a specific weight which will be honored when calculating the output. Training of a perceptron is mostly done by using backpropagation: At first the input data is propagated trough the neuronal network, then the difference of the expected to the current output is compared. This difference is propagated trough the network in the reversed order to adjust the weight of the neurons. This will result in a reduction of their influence on this specific input~\cite{Kriesel2007NeuralNetworks}. This process can be repeated to adjust the performance of the classifier. Those repetitions make the training of the classifier more complex (in terms of computational complexity), but the approach is faster for testing. Neuronal networks can be used as alternatives to conditional random fields or maximum-entropy models without performance loss~\cite{Collins:2002:DTM:1118693.1118694}.

\section{Tagsets\label{tagsets}}
There are different tagsets that can be used for part of a speech tagging. In this chapter we will describe the two different tagsets we used.

The \textbf{MSD Tagset} is the tagset originally used in the \multext{} corpus. It is a very fine-grained tagset that allows one to tag words very precisely.\\
A tag in this tagset usually starts with an '\#', is followed by one upper case character that indicates the category of the word and a number of lower case characters or digits that indicate attributes. The number of attributes can depend on the category of the word and the language.\\
For example a noun is tagged with an 'N' which indicates the category \texttt{noun}. In English it is followed by three attributes, the type ('c' for a common noun, 'p' for a proper noun), the gender and the number. In Romanian on the contrary it has six attributes. The first three are the same as in English while the following attributes indicate the case, definiteness and if it is a clitic.\\
To keep the tags as uniform as possible while still being able to take subtleties of different languages into account it is possible to omit attributes by using a dash in its place. The dash indicates that the property of the word is undecidable in the current language. After an arbitrary number of dashes the tag can continue assigning values to attributes. Dashes in the end of a tag can be omitted.\\
Lets consider 'winner', an English noun that has no gender. It would be tagged \texttt{\#Nc-s} as it is a common noun in singular. The word 'German' (meaning the language) would be tagged \texttt{\#Nc} because the attributes gender and number are undecidable.\\
Additionally to the MSD tags, \multexteast{} provides less specific POS-Tags and their mapping to the MSD tags in the file \texttt{msd2ctag.tbl}. The development of this mapping as been stopped, so the mapping is incomplete for almost all languages.

The \textbf{Universal Tagset} is a much simpler tagset than the MSD tagset. It was introduced by Slav Petrov et al.~\cite{universal}. It features only basic tags that indicate the category as indicated by the first character in the MSD tagset.
To be able to use this tagset a method mapping MSD tags to Universal tags was implemented by a mapping from MSD category to Universal tag.

The \textbf{Penn Treebank II Tagset} is more sophisticated than the Universal tagset but lacks the flexibility of the MSD tagset.
It doesn't have a modular system like the MSD tagset but fixed tags for different kinds words. So one can distinguish between proper and common nouns and the number is taken into account, but not the gender. The tagset may be handy to tag corpora in English but it obviously can't model all subtleties of all the other languages in the \multext{} corpus like Romanian or Farsi.

