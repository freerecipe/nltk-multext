% !TeX spellcheck = en_US
\chapter{Implementation}
In this section we describe our integration of \multexteast{} in \nltk{} and the on top implemented POS-Taggers. Due to problems with unicode in Python 2 we were forced to support Python 3 but as both versions are in use and Python 2 is not really deprecated we decided to write the code in a way that it can be used with both versions of Python. As \nltk{} version 2 does not support Python 3 we were forced to use \nltk{} 3. The older version is not supported due to incompatible interfaces and implementations, for example those of the Brill Tagger.\\
The additional POS-Tagger implemented in \scikit{} will also be discussed in this section.

\section{MULTEXT-East Integration in NLTK}
In order to be able to use the \multexteast{} corpus in \nltk{} like every other corpus, we implemented a custom \texttt{CorpusReader} and some other tools which provide conveniant features like the conversion of the MSD tagset to the Universal tagset, and a downloader which handles the installation of the \multexteast{} sources on the users' computer. While the corpus readerand the tagset conversion class reside in the file \texttt{mte.py}, the downloader is separate in a file called \texttt{MTEDownloader.py}.\\
Since commit \texttt{c6e7a6d5be33308c3c92e531124e93cd7f1908c4} in the \nltk{} repository at github\footnote{See \url{https://github.com/nltk/nltk}.} The corpus reader is shipped with \nltk{} and does not have to be downloaded separately. In the time we write this report, it is also worked on integrating the \multexteast{} sources into \nltk{}s own download tool. As this is not finished up to now, we provide the separate downloading tool ourselves.

\begin{figure}
\includegraphics[width=\textwidth]{./graphics/corpusreader_arch}
\caption{Overview over the architecture of the corpus reader for \multexteast{} inside \nltk{}}
\end{figure}

\subsection{The MTECorpusReader}
The \texttt{MTECorpusReader} extends the \texttt{TaggedCorpusReader} class of \nltk{}. As constructor argument one can choose which languages of the \multexteast{} corpus should be loaded. As all other corpus readers, the words, sentences, paragraphs, tagged words, and tagged sentences can be retrieved. Additionally we have methods that return the lemmatized words and sentences.\\
For all methods that return something with tags a filter can be given as parameter such that e.g. only words with \texttt{\#N--s} are in the returned list. The filter works in the way, that all \texttt{-} are seen as unspecified and can therefore have an arbitrary value, also if the given filter is too short it is filled up with \texttt{-} to the needed length internally. This means that with the example filter, for the English file, we would retrieve all singular nouns not regarding differences in gender or if the noun is proper or not.

\subsection{The MTETagConverter}
The \texttt{MTETagConverter} class contains the method for converting MSD tags into the matching Universal tags\footnote{A mapping for MSD to Universal tags is predefined by us and can be used out of the box}. If any other tagset is required a mapping file that contains a mapping from the MSD tagset to the target tagset can be added.\\
In this case it is important that one MSD tag is mapped to no more than one target tag. But multiple MSD tags can still map to the same target tag.\\

\subsection{The MTEDownloader}
The \texttt{MTEDownloader} is a standalone download manager. It can be either started via executing the script as a python program (it has a main method) or by directly calling \texttt{MTEDownloader.download()}. At first one has to choose the installation directory, then the corpus is downloaded from \url{clarin.si} and extracted. 

\subsection{Sample Usage of our Corpus Reader Implementation}
The following code shows some basic examples how the corpus reader, the Downloader and the provided utility methods could be used:
\begin{lstlisting}[basicstyle=\tiny]
> #at first import all necessary files
> import MTEDownloader, MTECorpusReader
>
> #then (if not yet done) download the Multext-East corpus
> MTEDownloader.download()
Where should the corpus be saved?[(0, '/home/stieglma/nltk_data'), (1, '/usr/share/nltk_data'), (2, '/usr/local/share/nltk_data'), (3, '/usr/lib/nltk_data'), (4, '/usr/local/lib/nltk_data'), (5, 'custom')] [0]: 0
Downloaded 14800805 of 14800805 bytes (100.00%)
Download finished
Extracting files...
Done
>
> # if you do not have an nltk version where our corpus reader is already
> # integrated, you have to manually create it
> # now we open the english version of the book 1984 with our reader
> reader =  mte.MTECorpusReader(root="/path/to/multext/corpus/", fileids=['oana-en.xml'])
>
> # otherwise you can just do the following
> from nltk.corpus import multext_east as reader
>
> # and then we retrieve the first word in the first word/tag tuple of this file
> reader.tagged_sents(fileids="oana-en.xml")[0][0]
('It', '#Pp3ns')
> 
> # the tag is now in the Multext-East (MSD) format, we want it to be 
> # the more well known corresponding universal tag:
> reader.tagged_sents(fileids="oana-en.xml", tagset="universal")[0][0][1]
'PRON'
>
> # now we want to see something in the concordance view:
> from nltk import Text
> Text(reader.words(fileids="oana-en.xml")).concordance("brother")
Displaying 2 of 80 matches:
ollow you about when you move . Big Brother is watching you , the caption benea
se-front immediately opposite . Big Brother is watching you , the caption said
\end{lstlisting}

\section{Part-of-Speech Taggers for \multexteast{}}
For the evaluation of our work we implemented Part-of-Speech taggers based on different algorithms, on the one hand we have Brill's algorithm, which is also shipped with \nltk{} and on the other hand we have three machine learning algorithms that are shipped with \scikit{}. The following sections will provide an overview over the implementations and show the way how they are used.

\subsection{The MTEBrillTagger}\label{impl:brill}
The implementation contains a wrapper around \nltk{}s implementation of the Brill Tagger. It builds a Brill Tagger based on a default tagger which can be specified. By default a unigramm tagger is used. Additionally it contains a method to evaluate the tagger.\\
The Brill Tagger for the \multexteast{} corpus can be configured like the standard \nltk{} Brill Tagger. Additionally the set of templates can be specified. Either a function from \texttt{nltk.tag.brill} returning a list of tags (e.g. \texttt{fntbl37}) can be specified as a string or a list of templates can be passed in the \texttt{template} parameter.

The \textbf{MTEBrillTagger} needs a set of tagged sentences to train the tagger. It is possible to tune the behaviour of the tagger by giving additional parameters like the maximum number of rules or a different initial tagger.

The \textbf{evaluate} method takes a set of test sentences to evaluates the already trained tagger. It prints the accuracy.

\label{impl:brill:metrics}The \textbf{metrics} method takes a set of sentences for testing and gives deeper information about the evaluation. There are accuracy, precision, recall, f-score and out of vocabulary words. Additionally a confusion matrix can be generated.

To evaluate the Brill Tagger there is the class \textbf{BrillTaggerEval}. It takes a whole corpus, tagged with MSD tags and will do the ten-crossfold-validation as described in section \ref{evaluation:10fold}. It also takes a string specifying the tagset as well as a few parameters to tune the Brill Tagger.
The \textbf{evaluate} method can do an n-crossfold-validation while the default for \texttt{n} is ten. The output will be the averages of the values specified in the metrics method of the MTEBrillTagger. Confusion matrices are not supported by this method at the moment.

\subsection{Part-of-Speech-Taggers with scikit-learn}
As a conclusion of the performance findings described in Section~\ref{background:mlscl} we chose a multinomial naive Bayes, a support vector machine with a linear kernel and a perceptron as classifiers for our implementation. For the implementation itself we have chosen \texttt{MultinomialNB}\footnote{\url{http://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.MultinomialNB.html\#sklearn.naive_bayes.MultinomialNB}}, \texttt{LinearSVC} \footnote{\url{http://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html\#sklearn.svm.LinearSVC}} and \texttt{Perceptron}\footnote{\url{http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Perceptron.html}}. All classifiers are configured with the default configuration. 

\paragraph{Loading the Corpus} is done by using \texttt{MTECorpusReader} for a given language. Our tagger uses the default \nltk{}-format or tagged sentences, so it can be easily changed to any other corpus.

\paragraph{Conversion of the Tagset} is optional but for comparison we decided to use the \textit{Universal} tagset and the default MSD tagset. The conversion itself is performed by a method within \texttt{MTETagConverter}.

\paragraph{Splitting the Data} is performed by our own implementation for a 10-fold-cross validation. For details, please refer to section~\ref{evaluation:10fold}.

\paragraph{Applying the Context Window} to the tagged sentences will result in the training vectors. The context window is specified as a list of positions, relative to the currently processed word within a sentence. This results in a dataset which maps the words, their position and tags within the context window to the tag of the currently processed word. An example  can be found in Figure \ref{fig:context_window}.

\begin{figure}
\includegraphics[width=\textwidth]{./graphics/context_window}
\caption{Application of the Context Window to a Sentence} \label{fig:context_window}
\end{figure}

\paragraph{Vectorization} is handled by \texttt{DictVectorizer} \footnote{\url{http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.DictVectorizer.html\#sklearn.feature_extraction.DictVectorizer}}, which vectorizes the data by a binary one-hot coding. It performs the different transformations for training- and test-data as described in the documentation.

\paragraph{Training the Classifier and Predicting the Results} is performed by calling the \texttt{fit} and \texttt{predict} method of the classifier. The first functions generates a model corresponding to the abstraction of the provided data where the second performs the classification of new, unknown data. 

\begin{figure}
	\includegraphics[width=\textwidth]{./graphics/scikit_pipeline}
	\caption{\scikit{} Part-of-Speech Tagger Overview}
\end{figure}


\subsection{Sample usage of our Part-of-Speech-Tagger Implementations}
\begin{lstlisting}[basicstyle=\tiny]
> from MTEPosTaggers.MTEBrillTagger import MTEBrillTagger
> from MTEPosTaggers.MTESKTagger import MTESKTagger
> from mte import MTECorpusReader


> loaded_corpus = MTECorpusReader(root="/path/to/multext/files/", fileids="oana-en.xml") #Loading English Language
> tagged_sents = loaded_corpus.tagged_sents() #get the tagged sents


> brill = MTEBrillTagger(tagged_sents) #instanciate the MTEBrillTagger with default settings

#omitted, training output brill tagger

> sktagger = MTESKTagger(tagged_sents) #instance the MTESKLeanTagger with default settings (MultinomialBayes)

> brill.evaluate(tagged_sents) #evaluate the model
0.9660261204763823

> sktagger.evaluate(tagged_sents) #same for the scikit tagger
0.8907907101624379
\end{lstlisting}

\section{Evaluation Framework}

\begin{figure}
	\includegraphics[width=\textwidth]{./graphics/evaluation_framework}
	\caption{Evaluation Framework Architecture}
\end{figure}


We decided to write a helper to perform the evaluation because there is a high amount of possible combinations:  languages $\times$ tagsets $\times$ taggers $\times$ configuration options. For each of this combination it is necessary to perform the n-folds.

Each tagger is represented by a class which implements \texttt{AbstractPoSTaggerImpl}. The abstract class contains an \texttt{evaluate} method and a public list named \texttt{config options}. This list contains the different configurations for the tagger. During the evaluation run, all options will be benchmarked. 

The \text{evaluate} method will be called for each combination of language, tagset and configuration option. As parameter it will get the tagged sentences for training and testing and the metric which should be returned as result.

Each implementation of a tagger ensures itself that the returned results are comparable, because the framework does not calculate the metrics itself. For our implementation of taggers we convert the results to the \nltk{}-format and return the calculated metrics by the \texttt{nltk.metrics.scores}\footnote{\url{http://www.nltk.org/_modules/nltk/metrics/scores.html}} module.

For the evaluation we use a 10-Fold cross validation where the minimum, maximum and averaged value (with standard deviation)  is stored in the results file.

\begin{figure}
	\includegraphics[width=\textwidth]{./graphics/evaluation_pipeline}
	\caption{Evaluation Pipeline Overview}
\end{figure}


\subsection{Sample usage of our Evaluation Framework}
To use the framework, each tagger has to meet the requirements stated above and be instanciatable from the \texttt{EvaluatePoSTaggers} script.\\
Prior to execution the following variables must be checked and if necessary, changed.
\begin{itemize}
	\item \texttt{result\_file}: Location of the results file.
	\item \texttt{languages}: List of languages to evaluate.
	\item \texttt{corpus\_root}: Root directory of the \multexteast-Corpus.
	\item \texttt{taggers}: List of taggers to evaluate.
	\item \texttt{metrics}: List of metrics to evaluate. Must be implemented by the \texttt{get\_result} function of each tagger.
	\item \texttt{n\_folds}: Number of folds for cross validation (min: 2)
	\item \texttt{n\_workers}: Number of workers for parallel execution (min: 1, max \texttt{n\_folds})
\end{itemize}

The following commented snipped shows how to use our brill-tagger within the framework:

\begin{lstlisting}[basicstyle=\tiny]
class nltkBrill(AbstractPoSTaggerImpl):
    config_options = ['fntbl37'] #define the configurations to evaluate
    max_rules = 250
    min_score = 2
    min_acc = None
    results = None

    def evaluate(self, sents_train, sents_test, config_option):   
        # start benchmarking training time
        t = time()
        
        # instanciation of the tagger + training
        brill = MTEBrillTagger(sents_train, max_rules=self.max_rules, min_score=self.min_score, min_acc=self.min_acc, template=config_option)
        
        # stop timekeeping for training
        self.training_time = time() - t
        t = time()
        
        # run tagging and store the results within instance
        self.results = brill.metrics(sents_test, printout=False)
        self.prediction_time = time() - t
        return self

    def get_result(self, metric):
        # return the corresponding results by
        # getting the right values out of the self.results
        # structure which is defined by MTEBrillTagger
        
        if metric == 'accuracy':
            return self.results[0]
        elif metric == 'precision':
            return self.results[1]
        elif metric == 'recall':
            return self.results[2]
        elif metric == 'f1':
            return self.results[3]
        elif metric == 'training_time':
            return self.training_time
        elif metric == 'prediction_time':
            return self.prediction_time
        elif metric == 'oov':
            return -1
\end{lstlisting}

